#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Marcus M. Scheunemann'
SITENAME = 'Marcus M. Scheunemann'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Dublin'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = \
#   (('Adaptive Systems Research Group', 'http://adapsys.cs.herts.ac.uk/'),
#	   ('Bold Hearts', 'https://robocup.herts.ac.uk'),
#    ('Nao Team Humboldt', 'https://www.naoteamhumboldt.de/'))

# Social widget
SOCIAL = \
    (('Google Scholar', 'https://scholar.google.de/citations?user=20odakMAAAAJ'),
     ('ORCID', 'https://orcid.org/0000-0002-0815-7024'),
     ('Twitter', 'https://twitter.com/mmscheunemann'),
	 	 ('LinkedIN','https://www.linkedin.com/in/scheunemann'),
     ('Github', 'https://github.com/scheunemann'),
     ('Gitlab', 'https://gitlab.com/scheunemann'),
#	   ('Stack Exchange', 'http://stackexchange.com/users/3997648/marcus'), #'stack-exchange'
#	   ('YouTube', 'https://www.youtube.com/MarcusScheunemann'),
     ('Researchgate', 'https://www.researchgate.net/profile/Marcus_Scheunemann'))

DEFAULT_PAGINATION = 10

# static paths will be copied without parsing their contents
STATIC_PATHS = [
    'publications',
    'extra/robots.txt',
    'extra/google_proof',
    'extra/bing_proof',
    'extra/favicon.ico',
    'extra/htaccess',
    'extra/portrait.jpg'
]

EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/google_proof': {'path': 'googlead6d13a95fee807f.html'},
    'extra/bing_proof': {'path': 'BingSiteAuth.xml'},
    'extra/favicon.ico': {'path':  'favicon.ico'},
    'extra/htaccess': {'path':  '.htaccess'},
    'extra/portrait.jpg': {'path': 'roboticist-marcus-scheunemann.jpg'}
}

##
# link structure
##

PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'

ARTICLE_SAVE_AS = '{slug}/index.html'
ARTICLE_URL = '{slug}/'

CATEGORY_URL = '{slug}'
CATEGORY_SAVE_AS =  '{slug}/index.html'

TAG_URL = 'tag/{slug}'
TAG_SAVE_AS = 'tag/{slug}/index.html'

## not created as single author page
AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''

CATEGORIES_URL = 'categories'
CATEGORIES_SAVE_AS = 'categories/index.html'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# path for seperate template files, used by, e.g., pelican-bibtex
# leave empty for now and just append in plugin configuration
THEME_TEMPLATES_OVERRIDES = []

# Plugins
PLUGIN_PATHS = ['plugins', '../pelican-plugins',  ]
PLUGINS = ['i18n_subsites', ]
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}

# use https://pypi.org/project/typogrify/
TYPOGRIFY = True
#TYPOGRIFY_IGNORE_TAGS = []

PYGMENTS_STYLE = 'murphy'
PYGMENTS_RST_OPTIONS = {'linenos': 'table', 'anchorlinenos' : 'true'}

##
## Plugin settings: pelican-youtube
##
# installed via pip
PLUGINS.append('pelican_youtube')


##
## Plugin settings: pelican-sitemap
##
PLUGINS.append('sitemap')
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.1,
        'pages': 0.8
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    },
    'exclude': ['tag/']
}

##
## Plugin settings: pelican-bootstrapify
##
PLUGINS.append('pelican-bootstrapify')

BOOTSTRAPIFY = {
    'table': ['table', 'table-striped', 'table-hover'],
    'img': ['img-fluid', 'border', 'border-primary'],
    'blockquote': ['blockquote'],
    #'code': ['badge badge-light']
}

##
## Plugin settings: pelican-bibtex
##

PLUGINS.append('pelican_bib')
PUBLICATIONS_SRC = 'content/publications/marcus-scheunemann.bib'
PUBLICATIONS_SPLIT_BY = 'tags'
PUBLICATIONS_UNTAGGED_TITLE = 'Others'

# add custom icon to all entries of menu items
# or a default one if non defined
#lst = list(MENUITEMS)
#for i,e in enumerate(lst):
#    if e[0] == "Publications":
#        lst[i] = (e[0], e[1], "edit")
#    else:
#        lst[i] = (e[0], e[1], "file-alt")
#MENUITEMS = tuple(lst)

##
## Theme settings: pelican-twitchy
##
# info: make sure plugin i18n_subsites is loaded, see Plugins

#THEME = 'themes/pelican-twitchy'
#HIDE_SITENAME = 'True'
#SHARE= 'True'
#BOOTSTRAP_THEME = 'mms'
#COOKIE_CONSENT = 'True'
#OPEN_GRAPH = 'True'

# show code and science links independend
## adding a custom base for adapting the sidebar
#THEME_TEMPLATES_OVERRIDES.append('templates/pelican-twitchy')
# add mms bootswatch theme from: https://gitlab.com/scheunemann/bootswatch-theme-mms
# STATIC_PATHS.append('bootswatch-theme-mms/bootstrap.min.css')
# EXTRA_PATH_METADATA.update({'bootswatch-theme-mms/bootstrap.min.css':{'path':'theme/css/bootstrap.mms.min.css'}})

##
## Theme settings: pelican-alchemy
##
import alchemy
THEME = alchemy.path()
THEME_TEMPLATES_OVERRIDES.append('templates/pelican-alchemy')
BOOTSTRAP_CSS = 'https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/yeti/bootstrap.min.css'
# TODO below 
#FONTAWESOME_CSS = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/fontawesome.min.css'
SITEIMAGE = 'roboticist-marcus-scheunemann.jpg width=160 height=160'
HIDE_AUTHORS = True
#DESCRIPTION = 'Some description ' \
#              'should go here.'
SITESUBTITLE = 'Postdoc who enables autonomous, curiosity-driven behavior generation in robots'
FOOTER_LINKS = \
    (('Adaptive Systems Research Group', 'http://adapsys.cs.herts.ac.uk/'),
     ('Bold Hearts', 'https://robocup.herts.ac.uk'),
     ('Nao Team Humboldt', 'https://www.naoteamhumboldt.de/'))

ICONS = \
		(('twitter', 'https://twitter.com/mmscheunemann'),
	 	 ('linkedin','https://www.linkedin.com/in/scheunemann'),
		 ('orcid', 'https://orcid.org/0000-0002-0815-7024'),
		 ('google', 'https://scholar.google.de/citations?user=20odakMAAAAJ'),	
	   ('researchgate', 'https://www.researchgate.net/profile/Marcus_Scheunemann'), 
     ('gitlab', 'https://gitlab.com/scheunemann'),
     ('github', 'https://github.com/scheunemann'))

