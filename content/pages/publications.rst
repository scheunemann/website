Publications
############
:template: publications
:lang: en

This page shows selected peer-reviewed publications. See also my external research profiles, e.g., `ORCID <https://orcid.org/0000-0002-0815-7024>`_, `arXiv <https://arxiv.org/a/scheunemann_m_1>`_, `Google Scholar <https://scholar.google.de/citations?user=20odakMAAAAJ>`_, `ResearchGate <https://www.researchgate.net/profile/Marcus_Scheunemann>`_, and `University of Hertfordshire <https://researchprofiles.herts.ac.uk/portal/en/persons/marcus-scheunemann(30cb9159-385c-4002-8225-d0115baf130a).html>`_.