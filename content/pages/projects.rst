Projects
########
:lang: en

This page will soon describe the science I was involved in the `Cognitive Robotics <https://koro.informatik.hu-berlin.de/>`_ group at the `Humboldt University of Berlin <https://hu-berlin.de/>`_ and the `Adaptive Systems Research Group <https://adapsys.cs.herts.ac.uk/>`_ at the `University of Hertfordshire <https://cs.herts.ac.uk/>`_. 

My projects involved robot grasping, applying in intrinsic motivation models to robots to have them generate interesting behavior. I measured the latter in human-robot interaction studies. I also participated in `RoboCup <https://robocup.org/>`_ (with my two teams `Nao Team Humboldt <https://naoth.de/>`_ and the `Bold Hearts <https://robocup.herts.ac.uk/>`_) and used that as a vehicle to study, e.g., deep learning for object recognition, reinforcement learning for motion generation or extending my software engineering skills (I extended the robotic operating system (`ROS 2 <https://ros,org/>`_) with packages to be able to use the framework on humanoid robots). I have a strong interest to study robots with minimal hardware, i.e., limited computational power.

I was also involved in constructing/maintaining sailplanes in the `Akademische Fliegergruppe Berlin <https://akaflieg-berlin.de/>`_ as well as in a few diving expeditions to discover new species of nudibranches, cowries and ovulidae.

Stay tuned :)