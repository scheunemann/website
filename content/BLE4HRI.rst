BLE4HRI
#######
:slug: BLE4HRI
:date: 2015-12-18 17:21
:modified: 2016-12-06 10:37

The code is hosted on `Gitlab <https://gitlab.com/scheunemann/BLE4HRI>`_ and `GitHub <https://github.com/scheunemann/BLE4HRI>`_.

.. youtube:: n1WmNfRdajo

More information in the corresponding `publication <{static}/publications/2016-Utilizing-Bluetooth-Low-Energy-to-recognize-proximity-touch-and-humans.pdf>`_.