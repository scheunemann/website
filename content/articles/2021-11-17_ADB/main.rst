Human Perception of Intrinsically Motivated Autonomy in Human-Robot Interaction
###############################################################################

:date: 2021-11-17 21:22
:tags: science
:category: misc
:status: hidden
:slug: adb2021

.. !!! Do not change the slug

These are the supplementary materials for the article "Human Perception of Intrinsically Motivated Autonomy in Human-Robot Interaction" published in `Adaptive Behavior <https://journals.sagepub.com/toc/ADB/current>`_.

------------

**Abstract:** A challenge in using robots in human-inhabited environments is to design 
behavior that is engaging, yet robust to the perturbations induced by human 
interaction. Our idea is to imbue the robot with intrinsic motivation (IM) so 
that it can handle new situations and appears as a genuine social other to 
humans and thus be of more interest to a human interaction partner.
Human-robot interaction (HRI) experiments mainly focus on scripted or 
teleoperated robots, that mimic characteristics such as IM to control isolated 
behavior factors. This article presents a "robotologist" study design that 
allows comparing autonomously generated behaviors with each other, and, for the 
first time, evaluates the human perception of IM-based generated behavior in 
robots.
We conducted a within-subjects user study (N=24) where participants 
interacted with a fully autonomous Sphero BB8 robot with different behavioral 
regimes: one realizing an adaptive, intrinsically motivated behavior and the 
other being reactive, but not adaptive.
The robot and its behaviors are intentionally kept minimal to concentrate on 
the effect induced by IM.
A quantitative analysis of post-interaction questionnaires showed a 
significantly higher perception of the dimension "Warmth" compared to the 
reactive baseline behavior. Warmth is considered a primary dimension for social 
attitude formation in human social cognition. A human perceived as 
warm (friendly, trustworthy) experiences more positive social interactions.

------------

The follwoing video shows the two conditions REA and ADA.

Video example of the two conditions
-----------------------------------

  .. youtube::  MchChhhNmkc
