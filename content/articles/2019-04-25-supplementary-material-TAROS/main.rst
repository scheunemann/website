Supplementary Material for "Human Perception of Predictive Information in Robots"
#################################################################################

:date: 2019-04-26 21:28
:modified: 2019-04-26 17:51
:tags: science
:category: misc
:slug: TAROS2019-supplementary

.. !!! Do not change the slug

These are the supplementary materials for the paper "Intrinsically Motivated Autonomy in Human-Robot Interaction: Human Perception of Predictive Information in Robots" to appear in the proceedings of the 20th Towards Autonomous Robotic Systems Conference (TAROS 2019) at Queen Mary University of London from the 3rd to the 5th of July 2019 (`official page <https://www.qmul.ac.uk/robotics/events/taros2019/>`_).

------------

**Abstract:** In this paper we present a fully autonomous and intrinsically motivated robot usable for HRI experiments.
We argue that an intrinsically motivated approach based on the Predictive Information formalism, like the one presented here, could provide us with a pathway towards autonomous robot behaviour generation, that is capable of producing behaviour interesting enough for sustaining the interaction with humans and without the need for a human operator in the loop.
We present a possible reactive baseline behaviour for comparison for future research. Participants perceive the baseline and the adaptive, intrinsically motivated behaviour differently.
In our exploratory study we see evidence that participants perceive an intrinsically motivated robot as less intelligent than the reactive baseline behaviour. We argue that is mostly due to the high adaptation rate chosen and the design of the environment. However, we also see that the adaptive robot is perceived as more warm, a factor which carries more weight in interpersonal interaction than competence.

------------

The following videos have been collected to have a preview for participants before doing the study. This hasn't been implemented in the study at the end, but they do show example behaviour of the robot.

At the end of each video, you see the motion pattern of the robot from a bird's eye perspective.

Video example of Adaptive behaviour
-----------------------------------

.. raw:: html

	<video data-dashjs-player src="PreviewAdaptive.mp4" controls>
		our browser does not support the video tag.
	</video>

[`Download video of adaptive behaviour <{attach}PreviewAdaptive.mp4>`_]

Video example of Reactive behaviour
-----------------------------------

.. raw:: html

	<video data-dashjs-player src="PreviewReactive.mp4" controls>
		our browser does not support the video tag.
	</video>

[`Download video of reactive behaviour <{attach}PreviewReactive.mp4>`_]

