Warmth and Competence to Predict Human Preference of Robot Behavior in Physical Human-Robot Interaction
#######################################################################################################

:date: 2020-07-27 14:28
:modified: 2020-09-01 17:35
:tags: science
:category: misc
:slug: ro-man2020

.. !!! Do not change the slug

The paper "Warmth and Competence to Predict Human Preference of Robot Behavior in Physical Human-Robot Interaction" will appear in the proceedings of the 29th IEEE International Conference on Robot and Human Interactive Communication (RO-MAN), held from 31/08 till 04/09/2020 (more on the `official page <http://ro-man2020.unina.it//>`_). Questions and discussions are always welcome! However, there is a slot for the live demo and question session scheduled for the 04/09 in the track `FRT72 <https://underline.io/events/28/sessions?eventSessionId=369>`_: `Creating human-robot relationships <https://underline.io/events/28/sessions?eventSessionId=369>`_.

Video
-----

.. raw:: html

	<video data-dashjs-player src="{attach}video.mp4" width="100%"  height="auto" controls>
		our browser does not support the video tag.
	</video>

[`Download video <{attach}video.mp4>`_]

This supplementary video shows the three conditions used in our experiment. All three conditions use the same robot platform, but the robot behavior generation differs by the used sensor input or by the update rules of the controller parameter (see the paper for details). Importantly, the behavioral regimes of the robot and resulting interactions look very similar.
All three conditions were presented to the participants and they were asked to fill in a questionnaire containing the dimensions of the Robotic Social Attribute Scale (i.e., Warmth, Competence, Discomfort) and the dimensions of the Godspeed scale (i.e., Anthropomorphism, Animacy, Likeability, Perceived Intelligence and Perceived Safety).
In social cognition, Warmth and Competence are considered universal dimensions for social attitude formation. We found that if a human participant responds to one robot behavior higher in Warmth (or Competence) compared to the other robot behaviors, this is a strong indicator that they prefer to interact with that robot again. This is similar to human-human interaction: a human who is perceived as more warm and competent experiences more positive interactions with their peers.
The findings are important for two reasons: (i) they indicate that the knowledge of Warmth and Competence transfers to HRI and (ii) they show that the dimensions can be used to predict human preference of robot behavior.

